package org.jenkinsci.plugins.restart;

import hudson.Extension;
import hudson.model.RootAction;
import org.kohsuke.stapler.DataBoundConstructor;

@Extension
public class RestartAction implements RootAction {

    @DataBoundConstructor
    public RestartAction() {
    }


    public String getIconFileName() {
        return "/images/48x48/star-gold.png";
    }

    public String getDisplayName() {
        return "Restart Jenkins";
    }

    public String getUrlName() {
        return "/restart";
    }

}

